package rivera.daniel.tl;

import rivera.daniel.bl.Camisa;
import rivera.daniel.bl.Catalogo;
import rivera.daniel.bl.Cliente;
import rivera.daniel.dl.CapaLogica;

import java.io.IOException;
import java.util.ArrayList;

public class CatalogoController {

    private CapaLogica logica;
    private Catalogo catalog;

    public CatalogoController() {
        logica = new CapaLogica();
        catalog = new Catalogo();
    }

    public String registrarCatalogo(String id, String nombre_Mes, String fechaDeCreacion) {
        Catalogo catalogo = new Catalogo(id,nombre_Mes,fechaDeCreacion);
        return logica.registrarCatalogo(catalogo);

    }

    public ArrayList<Catalogo> listarCatalogos() {
        return logica.listarCatalogos(); }

    public boolean crearArchivoCatalogos() throws IOException {
        if (logica.crearArchivoCatalogo() == true) {
            System.out.println("");
            return true;
        } else {
            System.out.println("El archivo ya está creado");
            return false;

        }
    }

    public Catalogo averiguarCatalogo(String id) {
        return logica.averiguarCatalogo(id);
    }

    public String agregarClienteACatalogo(String cedula, String id) {
        Catalogo cata = logica.averiguarCatalogo(id);
        if (cata != null) {
            Cliente cliente = logica.averiguarCliente(cedula);
            if (cliente != null) {
              ArrayList <Cliente> clientes = new ArrayList<>();
                   clientes.add(cliente);
                  cata.setClientes(clientes);
                return "Se agregó la camisa de manera correcta!";
            } else {
                return "El código de camisa: " + cedula + " no existe!";
            }
        }
        return "El catálogo indicado no existe";
    }

}

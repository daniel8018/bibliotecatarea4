package rivera.daniel.tl;

import rivera.daniel.bl.Cliente;
import rivera.daniel.dl.CapaLogica;

import java.io.IOException;
import java.util.ArrayList;

public class ClienteController {

    private CapaLogica logica;

    public ClienteController() {
        logica = new CapaLogica();
    }

    public String registrarCliente(String nombre, String apellido1, String apellido2, String dirreccionExacta, String correo, String cedula) {
        Cliente cliente = new Cliente(nombre, apellido1, apellido2, dirreccionExacta, correo, cedula);
        return logica.registrarCliente(cliente);

    }

    public ArrayList<Cliente> listarClientes() {
        return logica.listarClientes();
    }

    public boolean crearArchivoClientes() throws IOException {
        if (logica.crearArchivoCliente() == true) {
            System.out.println("");
            return true;

        } else {
            System.out.println("El archivo ya está creado");
            return false;

        }
    }

    public Cliente averiguarCliente(String cedula) {
        return logica.averiguarCliente(cedula);
    }

}
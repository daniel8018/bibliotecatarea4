package rivera.daniel.bl;

import java.util.Objects;

/**
 * @author Luis Daniel Rivera
 * @version 1.0.1
 * @since 1.0.1
 */
public class Camisa {
    //Atributos
    private String tam, descri, color;
    int id;
    private double precio;


    /**
         * Constructor vacio para la clase Camisa
         */
    public Camisa() {
    }// FIN DE CONSTRUCTOR POR DEFECTO

    /**
     * Constructor que recibe todos los parámetros de  Camisa y los Inicializa
     * @param tam Variable que es utilizada para idenfiticar el size  de la clase Camisa
     * @param descri Variable que es utilizada para idenfiticar la descripcion  de la clase Camisa
     * @param color Variable que es utilizada para idenfiticar el color  de la clase Camisa
     * @param id Variable que es utilizada para idenfiticar el id  de la clase Camisa
     * @param precio Variable que es utilizada para idenfiticar el precio  de la clase camisa
     */
    public Camisa(String tam, String descri, String color, int id, double precio ) {
        this.tam = tam;
        this.descri = descri;
        this.color = color;
        this.id = id;
        this.precio=precio;

    }

    /**
     * Metodo utilizado para obtener el atributo privado tam de la clase Camisa
     * @return El dato de retorno simboliza el size de la clase Camisa
     */
    public String getTam() {
        return tam;
    }

    /**
     * metodo utilizado para modifcar el valor de la variable privada tam  de la clase camisa
     * @param tam Variable que es utilizada para idenfiticar el size de la clase Camisa
     */
    public void setTam(String tam) {
        this.tam = tam;
    }

    /**
     * Metodo utilizado para obtener el atributo privado Descripcion de la clase camisa
     * @return El dato de retorno simboliza la descripcion de la camisa
     */
    public String getDescri() {
        return descri;
    }

    /**
     * metodo utilizado para modificar el valor de la variable privada descripcion de la clase Camisa
     * @param descri param Variable que es utilizada para idenfiticar la descripcion de la clase Camisa
     */
    public void setDescri(String descri) {
        this.descri = descri;
    }

    /**
     * Metodo utilizado para obtener el atributo privado color de la clase Camisa
     * @return El dato de retorno simboliza el color de la camisa
     */
    public String getColor() {
        return color;
    }


    /**
     * metodo utilizado para modificar el valor de la variable privada color de la clase Camisa
     * @param color Variable que es utilizada para idenfiticar el color  de la clase Camisa
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada Precio de la clase Camisa
     * @return la variable de retorno simboliza el precio de la camisa
     *
     */
    public double getPrecio() {
        return precio;
    }

    /**
     *metodo utilizado para modificar el valor de la variable privada  de la clase precio
     * @param precio Variable que es utilizada para idenfiticar el precio  de la clase precio
     */
    public void setPrecio(double precio) {
        this.precio = precio;
    }

    /**
     * Metodo utilizado para obtener el atributo privado id de la clase Camisa
     * @return El dato de retorno simboliza la id de la camisa
     */
    public int getId() {
        return id;
    }

    /**
     * metodo utilizado para modificar el valor de la variable privada id de la clase camisa
     * @param id Variable que es utilizada para identificar el id  de la clase camisa
     */
    public void setId(int id) {
        this.id = id;
    }

      /**
                * Metodo utilizado para imprimir todos los atributos de la clase camisa en un unico String
                * @return la variable de retorno simboliza todos los valores de la clase camisa en unico string
                */
    @Override
    public String toString() {
        return "Camisa{" +
                "tamaño='" + tam + '\'' +
                ", descripción='" + descri + '\'' +
                ", color='" + color + '\'' +
                ", id='" + id + '\'' +
                ", precio='" + precio + '\'' +
                '}';
    }

     /**
          * Método que compara los Atributos de la Clase - Para saber si los mismos se repiten
          * @param o es el objeto a comparar, para saber si es igual
          * @return Devuelve un booleano que indica si los atributos fueron repetidos o no.
          */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Camisa camisa = (Camisa) o;
        return getId() == camisa.getId();
    }

 /**
      * metodo utilizado para darle formato a el string para ser enviado al reader
      * @return El dato de retorno simboliza el string con el formato necesario para ser leido
      */
    public String toStringCSV(){
        return tam + "," + descri  + "," + color  + "," + id + "," + precio;
    }



}// FIN DE LA CLASE.
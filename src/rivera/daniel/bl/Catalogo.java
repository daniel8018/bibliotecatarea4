package rivera.daniel.bl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Objects;

/**
 * @author Luis Daniel Rivera
 * @version 1.0.1
 * @since 1.0.1
 */
public class Catalogo {
    //Atributos
    private String Id;
    private String nombre_Mes;
    private String fechaDeCreacion;
    private ArrayList<Camisa> camisas;
    private ArrayList<Cliente> clientes;

    /**
     * Constructor vacio para la clase Catalogo
     */
    public Catalogo() {
        clientes = new ArrayList<>();
    }// FIN DE CONSTRUCTOR POR DEFECTO.

    /**
     *
     * @param id Variable que simboliza el id de la clase catalogo
     * @param nombre_Mes Variable que simboliza el mes de la clase catalogo
     * @param fechaDeCreacion Variable que simboliza la fecha de creacion de la clase catalogo
     */
    public Catalogo(String id, String nombre_Mes, String fechaDeCreacion) {
        this.Id = id;
        this.nombre_Mes = nombre_Mes;
        this.fechaDeCreacion = fechaDeCreacion;
        this.clientes= new ArrayList<>();
        this.camisas = new ArrayList<>();
    }



    /**
     * metodo utilizado para obtener el valor de la variable privada Id de la clase Catalogo
     * @return la variable de retorno simboliza el id del catalogo
     */

    public String getId() {
        return Id;
    }

    public void addCliente(Cliente cliente){
        clientes.add(cliente);
    }

    /**
     * Metodo utilizado para modificar el atributo privado id de la clase Catalogo
     * @param id Variable que simboliza el id de la clase Catalogo
     */
    public void setId(String id) {
        Id = id;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada nombre_mes de la clase Catalogo
     * @return la variable de retorno simboliza el nombre del mes
     */
    public String getNombre_Mes() {
        return nombre_Mes;
    }

    /**
     * Metodo utilizado para modificar el atributo privado nombre_mes de la clase Catalogo
     * @param nombre_Mes Variable que simboliza el nombre del mes de la clase Catalogo
     */
    public void setNombreDelMeS(String nombre_Mes) {
        this.nombre_Mes = nombre_Mes;
    }

    /**
     *  metodo utilizado para obtener los datos del Arraylist privado de la clase Catalogo
     * @return la variable de retorno simboliza el Arraylist de clientes
     */
    public ArrayList<Cliente> getClientes() {
        return clientes;
    }

    /**
     * metodo utilizado para asignar valor a los datos del Arraylist privado de la clase Catalogo
     * @param clientes Variable que simboliza el Arraylist de clientes de la clase cliente
     */
    public void setClientes(ArrayList<Cliente> clientes) {
        this.clientes = clientes;
    }


    /**
     *  metodo utilizado para obtener los datos del Arraylist privado de la clase Catalogo
     * @return la variable de retorno simboliza el Arraylist de Camisa
     */
    public ArrayList<Camisa> getCamisas() {
        return camisas;
    }

    /**
     * metodo utilizado para asignar valor a los datos del Arraylist privado de la clase Catalogo
     * @param camisas Variable que simboliza el Arraylist de clientes de la clase camisas
     */
    public void setCamisas(ArrayList<Camisa> camisas) {
        this.camisas = camisas;
    }

    /**
     * metodo utilizado para registrar el objeto camisa y ser enviado al Arraylist
     * @param camisa objeto de la clase camisa
     */
    public void agregarCamisa(Camisa camisa) {
        camisas.add(camisa);
    }


    /**
     * Metodo utilizado para modificar el atributo privado fecha de creación de la clase Catalogo
     * @return
     */
    public String getFechaDeCreacion() {
        return fechaDeCreacion;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada fecha de creación de la clase Catalogo
     * @param fechaDeCreacion
     */
    public void setFechaDeCreacion(String fechaDeCreacion) {
        this.fechaDeCreacion = fechaDeCreacion;
    }

    /**
          * Metodo utilizado para imprimir todos los atributos de la clase Catalogo en un unico String
          * @return la variable de retorno simboliza todos los valores de la clase Catalogo en unico string
          */
    public String toString() {
        return "Catalogo{" +
                "Id='" + Id + '\'' +
                ", nombre= "+""+nombre_Mes +" "+ fechaDeCreacion+""+'}';
    }

 /**
      * Método que compara los Atributos de la Clase - Para saber si los mismos se repiten
      * @param o es el objeto a comparar, para saber si es igual
      * @return Devuelve un booleano que indica si los atributos fueron repetidos o no.
      */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Catalogo catalogo = (Catalogo) o;
        return getId().equals(catalogo.getId()) &&
                getNombre_Mes().equals(catalogo.getNombre_Mes()) &&
                getFechaDeCreacion().equals(catalogo.getFechaDeCreacion());
    }


    /**
     * metodo utilizado para darle formato a el string para ser enviado al reader
     * @return El dato de retorno simboliza el string con el formato necesario para ser leido
     */
    public String toStringCSV(){
        return Id + "," + nombre_Mes  + "," + fechaDeCreacion ;
    }


}// FIN DE PROGRAMA.



